<?php

namespace Pbritka\SpamProtection\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\InputException;

class CheckSpam implements ObserverInterface {

    /**
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime 
     */
    private $_dateTime;

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $_scopeConfig;

    /**
     *
     * @var \Magento\Framework\Message\Session 
     */
    private $_session;

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    private $_urlInterface;

    public function __construct(\Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Message\Session $session,
                                \Magento\Framework\UrlInterface $urlInterface) {
        $this->_dateTime = $dateTime;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $session;
        $this->_urlInterface = $urlInterface;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $request = $observer->getRequest();

        try {
            $this->_checkProtectField($request);
            
            $checkTimeActions = [
                'contact_index_post',
                'customer_account_createpost',
                'review_product_post'
            ];
            
            if (in_array($request->getFullActionName(), $checkTimeActions)) {
                $this->_checkTime();
            }
        } catch (InputException $ex) {
            $action = $observer->getControllerAction();
            $actionFlag = $action->getActionFlag();
            $actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
            $url = $this->_urlInterface->getUrl('spam_protection/error/index');
            $action->getResponse()->setRedirect($url);
        }
    }

    private function _checkProtectField($request) {
        $enableProtectField = $this->_scopeConfig->getValue('spam_protection/general/enable_protect_field', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $protectFieldName = $this->_scopeConfig->getValue('spam_protection/general/protect_field_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if ($enableProtectField and $protectFieldName) {
            $protectValue = $request->getPost($protectFieldName);

            if (strlen($protectValue) > 0 or is_null($protectValue)) {
                throw new InputException;
            }
        }
    }

    private function _checkTime() {
        $enableTimeCheck = $this->_scopeConfig->getValue('spam_protection/general/enable_time_check', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $minTime = $this->_scopeConfig->getValue('spam_protection/general/min_time_duration', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        
        if ($enableTimeCheck and $minTime > 0) {
            $startTime = $this->_session->getSpamProtectionStartTime();
            $now = $this->_dateTime->gmtTimestamp();
            
            if (!$startTime or $startTime > $now - $minTime) {
                throw new InputException;
            }
        }
    }

}
