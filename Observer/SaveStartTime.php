<?php

namespace Pbritka\SpamProtection\Observer;

use Magento\Framework\Event\ObserverInterface;

class SaveStartTime implements ObserverInterface {

    /**
     *
     * @var \Magento\Framework\Stdlib\DateTime\DateTime 
     */
    private $_dateTime;

    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface 
     */
    private $_scopeConfig;

    /**
     *
     * @var \Magento\Framework\Message\Session 
     */
    private $_session;

    public function __construct(\Magento\Framework\Stdlib\DateTime\DateTime $dateTime,
                                \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
                                \Magento\Framework\Message\Session $session) {
        $this->_dateTime = $dateTime;
        $this->_scopeConfig = $scopeConfig;
        $this->_session = $session;
    }

    public function execute(\Magento\Framework\Event\Observer $observer) {
        $enableTimeCheck = $this->_scopeConfig->getValue('spam_protection/general/enable_time_check', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if ($enableTimeCheck) {
            $now = $this->_dateTime->gmtTimestamp();
            $this->_session->setSpamProtectionStartTime($now);
        }
    }

}
