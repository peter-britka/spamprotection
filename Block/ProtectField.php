<?php

namespace Pbritka\SpamProtection\Block;

class ProtectField extends \Magento\Framework\View\Element\Template {
    /**
     * Return the name of the hidden field
     * 
     * @access public
     * @return string
     */
    public function getProtectFieldName() {
        return $this->_scopeConfig->getValue('spam_protection/general/protect_field_name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}